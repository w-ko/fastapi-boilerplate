import abc
from dataclasses import dataclass


@dataclass
class Command(abc.ABC):
    # This class serves as a base class for all commands
    pass
