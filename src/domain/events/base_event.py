import abc
from dataclasses import dataclass


@dataclass
class Event(abc.ABC):
    # This class serves as a base class for all events
    pass
